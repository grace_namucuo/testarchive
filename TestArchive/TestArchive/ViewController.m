//
//  ViewController.m
//  TestArchive
//
//  Created by 戴运鹏 on 2019/3/17.
//  Copyright © 2019 戴运鹏. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor redColor];
}


@end
